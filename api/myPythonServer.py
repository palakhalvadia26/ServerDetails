import sys
import paramiko
import getpass
import os
import webbrowser
import pandas as pd
from flask_api import FlaskAPI
from flask import Flask, request, redirect, url_for, send_from_directory
from pymongo import MongoClient
import json
import numpy as np
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = set(['txt', 'js', 'json', 'html'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

remote_conn_pre = paramiko.SSHClient()


def connection(username, password, host_address):
    try:
        print('try')
        remote_conn_pre.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        remote_conn_pre.connect(host_address, username=username,
                            password=password, look_for_keys=False, allow_agent=False)
        return remote_conn_pre
    except:
        print('catch')
        return 0


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods',
                         'GET,PUT,POST,DELETE,OPTIONS')
    return response


@app.route('/details', methods=['POST'])
def showMachineList():

    python_obj = json.dumps(request.get_json())
    python_obj = json.loads(python_obj)
    df = pd.io.json.json_normalize(python_obj)
    df.columns = df.columns.map(lambda x: x.split(".")[-1])
    username = df['username'][0]
    URL = df['URL'][0]
    password = df['password'][0]
    ip = df['ipAddress'][0]
    path = ''
    remote_conn_pre = connection(username, password, ip)
    if(remote_conn_pre!=0):
        print('congo')        
        return 'searchServer'
    else:
        print('else')
        
        return 'Invalid'        

@app.route('/getPath', methods=['POST','GET'])
def getPath():
    path = json.dumps(request.get_json())
    path = json.loads(path)
    df = pd.io.json.json_normalize(path)
    df.columns = df.columns.map(lambda x: x.split(".")[-1])
    directory = df['path'][0]
    print(directory)
    stdin, stdout, stderr = remote_conn_pre.exec_command("find "+directory)
    dirs = []
    for line in stdout.readlines():
        dirs.append(line.strip())
    return str(json.dumps(dirs))
 

@app.route('/getFile', methods=['POST'])
def getFile():
    path = json.dumps(request.get_json())
    path = json.loads(path)
    df = pd.io.json.json_normalize(path)
    df.columns = df.columns.map(lambda x: x.split(".")[-1])
    directory = df['path'][0]
    # get data from post
    get_file_path = json.dumps(request.get_json())
    get_file_path = json.loads(get_file_path)
    df = pd.io.json.json_normalize(get_file_path)
    df.columns = df.columns.map(lambda x: x.split(".")[-1])
    file_name= str(str(directory).split('/')[-1])
    ftp = remote_conn_pre.open_sftp()
    ftp.get(''+directory+'', file_name)
    ftp.close()
  # webbrowser.open_new(os.getcwd()+"/create_user.js")
    return 'Download'

@app.route('/enterPath', methods=[ 'POST'])
def enterPath():
    fileUploadPath = json.dumps(request.get_json())
    # # request.method == 'POST'
    fileUploadPath = json.loads(fileUploadPath)
    put_file_path = pd.io.json.json_normalize(fileUploadPath)
    put_file_path.columns = put_file_path.columns.map(lambda x: x.split(".")[-1])
    folder = put_file_path['uploadPath'][0]
    # print(str(folder).split('/')[-1])
    
    ftp = remote_conn_pre.open_sftp()
    ftp.put('saurabh.txt', ''+folder+'/'+str(folder).split('/')[-1])
    ftp.close()

    return 'Uploaded'
    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
