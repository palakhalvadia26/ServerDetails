import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'app-server-listing',
  templateUrl: './server-listing.component.html',
  styleUrls: ['./server-listing.component.css']
})
export class ServerListingComponent implements OnInit {
path;
auto_complite_path;
uploadPath:string; 
pathToUpload:string
constructor( public http: Http) {
console.log('saurabh');

   }

  ngOnInit() {
  }
  getPath() {
  console.log(this.path);
  var path_json={
    "path":this.path
  }
    this.http.post('http://127.0.0.1:5000/getPath', path_json)

      .subscribe(isPresent => {
        console.log(isPresent['_body']);
        this.auto_complite_path=JSON.parse (isPresent['_body']);

      });

  }

  showFile(){
    var path_json={
      "path":this.path
    }
    this.http.post('http://127.0.0.1:5000/getFile', path_json)

    .subscribe(isPresent => {
      console.log(isPresent);
    });
  }
 
  uploadFile(){
    // var uploadPath = this.uploadPath
    var uploadPath_json={
      "uploadPath":this.pathToUpload
    }
    this.http.post('http://127.0.0.1:5000/enterPath', uploadPath_json)
    .subscribe(isPresent => {
      console.log(isPresent);
    })
  }

}
