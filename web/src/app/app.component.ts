import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { SessionStorage, LocalStorage, LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  @LocalStorage()
  public url;

  constructor(private router: Router, public storage: LocalStorageService) {

    this.storage.store('url', 'http://localhost:3000/api/');
  
  }
}
