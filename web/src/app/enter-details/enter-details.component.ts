import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { ServerListingComponent } from '../server-listing/server-listing.component';
import { Http } from '@angular/http';
import { SessionStorage, LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { URLSearchParams } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-enter-details',
  templateUrl: './enter-details.component.html',
  styleUrls: ['./enter-details.component.css']
})

export class EnterDetailsComponent implements OnInit {

  url;
  error:string;
  serverDetails = {
    URL: '',
    ipAddress: '',
    username: '',
    password: ''
  };

  constructor(public http: Http, public router: Router, public storage: LocalStorageService) {
    this.url = this.storage.retrieve('url');

  }

  ngOnInit() {
  }
  // getPath() {
  //   var path = "/var/www/"
  //   this.http.post('http://127.0.0.1:5000/getPath', path)
  //     .subscribe(isPresent => {
  //       console.log(isPresent);
  //     });

  // }
  searchServer(serverDetails) {
    this.http.post('http://127.0.0.1:5000/details', serverDetails)
      .subscribe(isPresent => {
         if(isPresent['_body']=='searchServer'){
           this.router.navigate(['/server-listing'])
          }
          else{
            this.error='Invalid Credentials'
         }
      });
  }
}
