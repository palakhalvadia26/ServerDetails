import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { EnterDetailsComponent } from './enter-details/enter-details.component';
import { ServerListingComponent } from './server-listing/server-listing.component';
import { Ng2Webstorage } from 'ngx-webstorage';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';


const appRoutes: Routes = [
  { path: '', component: EnterDetailsComponent },
  { path: ':serverListing', component: ServerListingComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    EnterDetailsComponent,
    ServerListingComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
    ),
    RouterModule,
    Ng2Webstorage,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

